﻿
#include <iostream>
#include <fstream>
#include <vector>
#include <string>

class ListNode
{
public:
    ListNode* Prev;
    ListNode* Next;
    ListNode* Rand; // произвольный элемент внутри списка
    std::string Data;

    ListNode(const std::string& data) : Prev(nullptr), Next(nullptr), Rand(nullptr), Data(data) {}

};


class ListRand
{
public: 
    ListNode* Head;
    ListNode* Tail;
    int Count;

    void Serialize(std::ofstream& file)
    {
        file << Count << "\n";

        ListNode* current = Head;

        while (current != nullptr)
        {
            file << current->Data << "\n";
            file << getPosition(current->Prev) << "\n";
            file << getPosition(current->Next) << "\n";
            file << getPosition(current->Rand) << "\n";
            current = current->Next;
        }

    }

    void Deserialize(std::ifstream& file)
    {

        std::string line;

        getline(file, line);
        int count = std::stoi(line);

        std::vector<ListNode*> nodes(count);
        for (int i = 0; i < count; i++)
        {
            nodes[i] = new ListNode("");
        }

        for (int i = 0; i < count; i++)
        {
            getline(file, line);
            std::string data = line;

            getline(file, line);
            int prevIndex = std::stoi(line);

            getline(file, line);
            int nextIndex = std::stoi(line);

            getline(file, line);
            int randIndex = std::stoi(line);

            nodes[i]->Data = data;

            if (prevIndex != -1)
                nodes[i]->Prev = nodes[prevIndex];
            if (nextIndex != -1)
                nodes[i]->Next = nodes[nextIndex];
            if (randIndex != -1)
                nodes[i]->Rand = nodes[randIndex];
            
        }

        Head = count != 0 ? nodes.front(): nullptr;
        Tail = count != 0 ? nodes.back() : nullptr;
        Count = count;
        }

private:
    int getPosition(ListNode* node)
    {
        if (node == nullptr)
            return -1;

        ListNode* current = Head;
        int position = 0;
        while (current != nullptr)
        {
            if (current == node)
                return position;

            position++;
            current = current->Next;
        }
        return -1;
    }
};    



int main()
{
    //Let's create and initialize node's list
    ListRand list;
    list.Count = 0;
    //Let's create and initialize 3 nodes as an example

    ListNode* node1 = new ListNode("Node 1");
    ListNode* node2 = new ListNode("Node 2");
    ListNode* node3 = new ListNode("Node 3");

    //Let the nodes go one by one and have pseudo-random references to other nodes
    node1->Next = node2;
    node2->Prev = node1;
    node2->Next = node3;
    node3->Prev = node2;

    node1->Rand = node3;
    node2->Rand = node1;
    node3->Rand = node2;

    list.Head = node1;
    //list.Tail = node3;

    //Find Tail and Count
    ListNode* current = list.Head;
    while (current != nullptr)
    {
        current = current->Next;
        list.Count++;
    }
    list.Tail = current;

    //Serialize list into file
    std::ofstream outFile("list_data.txt");
    if (outFile.is_open())
    {
        list.Serialize(outFile);
        outFile.close();
        std::cout << "Serialization successfully complete" << std::endl;
    }
    else
    {
        std::cerr << "Failed to open file for writing" << std::endl;
    }
        
    ListRand deserializedList;
    std::ifstream inFile("list_data.txt");
    if (inFile.is_open())
    {
        deserializedList.Deserialize(inFile);
        inFile.close();
        std::cout << "Deserialization successfully complete" << std::endl;


        // Check deserialization results
        std::cout << "Deserialized list" << std::endl;
        ListNode* currentNode = deserializedList.Head;

        while (currentNode != nullptr)
        {
            std::cout << "Data: " << currentNode->Data << std::endl;
            std::cout << "Prev: " << ((currentNode->Prev != nullptr) ? currentNode->Prev->Data : "nullptr") << std::endl;
            std::cout << "Next: " << ((currentNode->Next != nullptr) ? currentNode->Next->Data : "nullptr") << std::endl;
            std::cout << "Rand: " << ((currentNode->Rand != nullptr) ? currentNode->Rand->Data : "nullptr") << std::endl;

            std::cout << std::endl;
            currentNode = currentNode->Next;
        }
    }
    else
    {
        std::cerr << "Failed to open file for reading";
    }

    //memory deallocating
    ListNode* currentNode = list.Head;
    while (currentNode != nullptr)
    {
        ListNode* nextNode = currentNode->Next;
        delete currentNode;
        currentNode = nextNode;
    }
    list.Head = nullptr;
    list.Tail = nullptr;
    list.Count = 0;

    return 0;


}


